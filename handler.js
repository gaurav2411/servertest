'use strict';
const serverless=require('serverless-http')
const server=require('./index')
const handler=serverless(server)
module.exports.server=async (event,context)=> await handler(event,context)

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
